#ifndef SRC_SCENE_H

#define SRC_SCENE_H

#include <string>
#include <vector>

#include "Couleur.h"
#include "Intersection.h"
#include "Rayon.h"
#include "Source.h"

class Objet;

class Scene {
 public:
    Scene(std::string filename);
    ~Scene();
    void Print();
    bool Intersecte(const Rayon& r, Intersection& inter);

    inline Couleur GetFond() { return fond; }

 private:
    Couleur fond;
    std::vector<Objet*> objets;
    Source source;
};

#endif  // SRC_SCENE_H
