#ifndef SRC_SPHERE_H

#define SRC_SPHERE_H

#include <iostream>
#include <cmath>

#include "Objet.h"
#include "Point.h"
#include "Rayon.h"

class Sphere : public Objet {
 public:
    Point C;
    float R;

    Sphere(Materiau mat = Materiau(), Point centre = Point(), float rayon = 1.0) : Objet(mat), C(centre), R(rayon) {
		
    }

    virtual bool Intersect(const Rayon& r, Intersection& inter) override {
        float a = pow(r.d.DX, 2) + pow(r.d.DY, 2) + pow(r.d.DZ, 2);
        float b = 2 * ( r.d.DX * (r.o.X - C.X) +
                        r.d.DY * (r.o.Y - C.Y) +
                        r.d.DZ * (r.o.Z - C.Z) );
        float c = pow((r.o.X - C.X), 2) +
                  pow((r.o.Y - C.Y), 2) +
                  pow((r.o.Z - C.Z), 2) - R * R;

        float delta = b * b - 4 * a * c;

        if (delta < 0) {
            return false;
        } else if (delta == 0) {
            float t = - b / 2 * a;

            Point p(r.o.X + t * r.d.DX,
                    r.o.Y + t * r.d.DY,
                    r.o.Z + t * r.d.DZ);

            inter = Intersection(p, this, t);
            return true;

        } else if (delta > 0) {
            float t1 = (- b - sqrt(delta)) / 2 * a;
            float t2 = (- b + sqrt(delta)) / 2 * a;

            float t = std::min(t1, t2);
            Point p(r.o.X + t * r.d.DX,
                    r.o.Y + t * r.d.DY,
                    r.o.Z + t * r.d.DZ);

            inter = Intersection(p, this, t);

            return true;
        }

        return false;
    }

    virtual void Print() {
        std::cout << "Sphere : de rayon " << R << ", de centre ";
        C.Print();
        std::cout << " de matériau ";
        Mat.Print();
    }
};

#endif  // SRC_SPHERE_H
