#ifndef SRC_INTENSITE_H

#define SRC_INTENSITE_H

#include <iostream>

class Intensite {
 public:
    float Rouge;
    float Vert;
    float Bleu;

    Intensite(float r = 1.0, float v = 1.0, float b = 1.0) {
        Rouge = r;
        Vert = v;
        Bleu = b;
    }

    void Print() {
        std::cout << "(" << Rouge << "," << Vert << "," << Bleu << ")";
    }
};

#endif  // SRC_INTENSITE_H
