#include "Camera.h"

#include "Objet.h"
#include "Rayon.h"
#include "Vecteur.h"

Camera::Camera() :
    pos(0.0, 0.0, 2.0),
    distance(-2.0) {

}

void Camera::GenererImage(Scene& sc, Image& img) {
    float largeurPixel = 2.0 / img.GetLargeur();
    float hauteurPixel = 2.0 / img.GetHauteur();

    for (int j = 0; j < img.GetHauteur(); j++) {
        for (int i = 0; i < img.GetLargeur(); i++) {
            float milieuPixelX = static_cast<float>(i) /
                                 static_cast<float>(img.GetLargeur()) *
                                 2.0 - 1.0;
            milieuPixelX += largeurPixel / 2.0;

            float milieuPixelY = static_cast<float>(j) /
                                 static_cast<float>(img.GetHauteur()) *
                                 2.0 - 1.0;
            milieuPixelY += hauteurPixel / 2.0;
            milieuPixelY *= -1.0;


            Vecteur d(milieuPixelX, milieuPixelY, distance);
            d.Normalize();
            Rayon r(pos, d);

            if (i == 152 && j == 46) {
                std::cout << "Prout" << std::endl;
            }

            Intersection isec;
            if (sc.Intersecte(r, isec)) {
                img.SetPixel(i, j, isec.Obj->Mat.Albedo);
            } else {
                img.SetPixel(i, j, sc.GetFond());
            }

            if ( (i == 0    && j == 0   ) ||
                 (i == 12   && j == 7   ) ||
                 (i == 101  && j == 200 ) ) {
                //pixel (0,0) : rayon = (0,0,2) -> (-0.996094,0.996094,-2)
                std::cout << "pixel (" << i << ", " << j << ") : rayon = ";
                r.Print();
                std::cout << std::endl;
            }
        }
    }
}
