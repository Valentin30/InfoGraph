#include "Camera.h"
#include "Image.h"
#include "Scene.h"

int main(int argc, char** argv) {
    Scene scene("scene03.txt");
    scene.Print();

    Camera cam;

    Image img(256,256);

    cam.GenererImage(scene, img);

    img.Sauver("Test.ppm");

    return 0;
}
