#ifndef SRC_CAMERA_H

#define SRC_CAMERA_H

#include <string>

#include "Image.h"
#include "Point.h"
#include "Scene.h"

class Camera {
 public:
    Camera();
    void GenererImage(Scene& sc, Image& img);

 private:
    Point pos;
    float distance;
};

#endif  // SRC_CAMERA_H
