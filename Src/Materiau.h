#ifndef SRC_MATERIAU_H

#define SRC_MATERIAU_H

#include <iostream>

#include "Couleur.h"

class Materiau {
 public:
    Couleur Albedo;
    float Kd;
    float Ks;
    float S;

    Materiau(Couleur albedo = Couleur(0.8, 0.8, 0.8), float kd = 0.5, float ks = 0.1, float s = 10) {
        Albedo = albedo;
        Kd = kd;
        Ks = ks;
        S = s;
    }

    void Print() {
        std::cout << "[";
        Albedo.Print();
        std::cout << "," << Kd << "," << Ks << "," << S << "]";
    }
};

#endif  // SRC_MATERIAU_H
