#ifndef SRC_INTERSECTION_H

#define SRC_INTERSECTION_H

#include "Point.h"

class Objet;

class Intersection : Point {
 public:
    Intersection(Point p, Objet* o, float t) :
        Point(p.X, p.Y, p.Z),
        Obj(o),
        Dist(t) {

    }

    Intersection() { }

    Objet* Obj;
    float Dist;
};

#endif  // SRC_INTERSECTION_H
