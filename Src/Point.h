#ifndef SRC_POINT_H

#define SRC_POINT_H

#include <iostream>

class Point {
 public:
    float X;
    float Y;
    float Z;

    Point(float x = 0.0, float y = 0.0, float z = 0.0) {
        X = x;
        Y = y;
        Z = z;
    }

    void Print() {
        std::cout << "(" << X << "," << Y << "," << Z << ")";
    }
};

#endif  // SRC_POINT_H
