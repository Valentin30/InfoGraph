#ifndef SRC_IMAGE_H

#define SRC_IMAGE_H

#include <string>

#include "Couleur.h"

class Image {
 public:
    Image();
    Image(int l, int h);
    ~Image();

    inline int GetLargeur() const { return largeur; }
    inline int GetHauteur() const { return hauteur; }

    void SetPixel(int x, int y, Couleur c);
    Couleur GetPixel(int x, int y);

    bool Sauver(std::string filename);

 private:
    Couleur* pixel;
    int largeur;
    int hauteur;
};

#endif  // SRC_IMAGE_H
