#include "Image.h"

#include <fstream>

Image::Image() :
    largeur(256),
    hauteur(256) {
    pixel = new Couleur[largeur * hauteur];
}

Image::Image(int l, int h) :
    largeur(l),
    hauteur(h) {
    pixel = new Couleur[largeur * hauteur];
}

void Image::SetPixel(int x, int y, Couleur c) {
    pixel[x + y * largeur] = c;
}

Couleur Image::GetPixel(int x, int y) {
    return pixel[x + y * largeur];
}

Image::~Image() {
    delete[] pixel;
}

bool Image::Sauver(std::string filename) {
    std::ofstream ofs(filename, std::ofstream::out);

    if (!ofs.is_open())
        return false;

    ofs << "P3" << std::endl;
    ofs << largeur << " " << hauteur << std::endl;
    ofs << 255 << std::endl;
    for (int j = 0; j < hauteur; j++) {
        for (int i = 0; i < largeur; i++) {
            Couleur c = GetPixel(i, j);
            ofs << c.GetRougei() << " " <<
                   c.GetVerti() << " " <<
                   c.GetBleui() << std::endl;
        }
    }

    ofs.close();

    return true;
}
