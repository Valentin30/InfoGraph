#ifndef SRC_OBJET_H

#define SRC_OBJET_H

#include <iostream>

#include "Intersection.h"
#include "Materiau.h"
#include "Rayon.h"

class Objet {
 public:
    Materiau Mat;

    Objet(Materiau mat = Materiau()) {
        Mat = mat;
    }

    virtual bool Intersect(const Rayon& r, Intersection& inter) = 0;
    virtual void Print() = 0;
};

#endif  // SRC_OBJET_H
