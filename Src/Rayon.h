#ifndef SRC_RAYON_H

#define SRC_RAYON_H

#include <iostream>

#include "Point.h"
#include "Vecteur.h"

class Rayon {
 public:
    Point o;
    Vecteur d;

    Rayon() { }
    Rayon(Point origin, Vecteur direction) :
        o(origin),
        d(direction) {
    }

    void Print() {
        std::cout << "(" << o.X << "," << o.Y << "," << o.Z << ") -> "
                     "(" << d.DX << "," << d.DY << "," << d.DZ << ")";
    }
};

#endif  // SRC_POINT_H
