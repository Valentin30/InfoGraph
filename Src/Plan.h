#ifndef SRC_PLAN_H

#define SRC_PLAN_H

#include <iostream>

#include "Objet.h"
#include "Rayon.h"

class Plan : public Objet {
 public:
    float A;
    float B;
    float C;
    float D;

    Plan(Materiau mat = Materiau(), float a = 0.0, float b = 1.0, float c = 0.0, float d = 0.0) : Objet(mat), A(a), B(b), C(c), D(d) {

    }

    virtual bool Intersect(const Rayon& r, Intersection& inter) override {
        float haut = - (A * r.o.X + B * r.o.Y + C * r.o.Z + D);
        float bas = (A * r.d.DX + B * r.d.DY + C * r.d.DZ);

        if (bas == 0)
            return false;

        float t = haut / bas;

        if (t < 0)
            return false;

        Point p(r.o.X + t * r.d.DX,
                r.o.Y + t * r.d.DY,
                r.o.Z + t * r.d.DZ);

        inter = Intersection(p, this, t);

        return true;
    }

    virtual void Print() {
        std::cout << "Plan : d'équation " << A << " x + " << B << " y + " << C << " z + " << D << " = 0 ";
        std::cout << " de matériau ";
        Mat.Print();
    }
};

#endif  // SRC_PLAN_H
